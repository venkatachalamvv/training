	 SELECT emp.first_name
           ,emp.surname
		   ,dob
	       ,dept.department_name
           ,annual_salary
	   FROM exercise2.employee emp 
           ,exercise2.department dept
	  WHERE DATE_FORMAT(dob,'%d-%m') = DATE_FORMAT(SYSDATE(),'%d-%m');