CREATE SCHEMA exercise2;
CREATE TABLE `exercise2`.`department` (
            `department_number` INT NOT NULL
		   ,`department_name` VARCHAR(45) NOT NULL
            ,PRIMARY KEY (`department_number`));
INSERT INTO `exercise2`.`department` (`department_number`, `department_name`) 
     VALUES ('1', 'ITDesk');
INSERT INTO `exercise2`.`department` (`department_number`, `department_name`) 
	 VALUES ('2', 'Finance');
INSERT INTO `exercise2`.`department` (`department_number`, `department_name`) 
     VALUES ('3', 'Engineering');
INSERT INTO `exercise2`.`department` (`department_number`, `department_name`) 
	 VALUES ('4', 'HR');
INSERT INTO `exercise2`.`department` (`department_number`, `department_name`) 
	 VALUES ('5', 'Recruitment');
INSERT INTO `exercise2`.`department` (`department_number`, `department_name`) 
     VALUES ('6', 'Facility');
CREATE TABLE `exercise2`.`employee` (
             `emp_id` INT NOT NULL
            ,`first_name` VARCHAR(45) NOT NULL
            ,`surname` VARCHAR(3) NOT NULL
            ,`dob` DATE NOT NULL
            ,`date_of_joining` DATE NOT NULL
            ,`annual_salary` INT NOT NULL
            ,`department_number` INT NOT NULL
            ,`area` VARCHAR(45) NOT NULL
			,PRIMARY KEY (`emp_id`)
            ,INDEX `department_number_idx` (`department_number` ASC) VISIBLE
            ,CONSTRAINT `department_number`
	 FOREIGN KEY (`department_number`)
  REFERENCES `exercise2`.`department` (`department_number`)
   ON DELETE RESTRICT
   ON UPDATE CASCADE);
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('1', 'Ram', 'A', '1992/02/03', '2015/04/12', '1200000', '1','Arasur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('2', 'Mani', 'S', '2000/02/25', '2019/11/05', '1000000', '1','Arasur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('3', 'Praveen', 'D', '1999/05/17', '2016/10/12', '900000', '1','GANDhipuram');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('4', 'Rahul', 'F', '2000/12/12', '2019/04/16', '800000', '1','Karumathampatti');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('5', 'Manoj', 'G', '1996/09/05', '2016/03/02', '800000', '1','Ganapathy');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('6', 'Venki', 'H', '1997/08/15', '2016/02/11', '1000000', '2','GANDhipuram');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('7', 'Naveen', 'J', '1996/06/25', '2017/01/12', '900000', '2','Karumathampatti');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('8', 'Karthi', 'K', '1997/01/04', '2018/06/09', '1000000', '2','Karumathampatti');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('9', 'Thangavel', 'L', '1997/09/04', '2016/10/02', '800000', '2','Arasur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('10', 'Venkatesh', 'P', '1995/02/27', '2015/04/25', '900000', '2','Ganapathy');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('11', 'Thiru', 'O', '1992/03/25', '2018/06/09', '1000000', '3','Thudiyalur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('12', 'Murugan', 'I', '1996/05/05', '2019/06/19', '1000000', '3','Arasur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('13', 'Venkat', 'A', '1997/05/04', '2017/06/29', '800000', '3','Arasur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('14', 'Senthil', 'Y', '1997/03/14', '2018/05/08', '1000000', '3','Karumathampatti');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('15', 'Gopi', 'T', '1998/02/04', '2016/06/01', '1000000', '3','Ganapathy');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('16', 'Guru', 'R', '1997/11/24', '2017/07/05', '900000', '4','GANDhipuram');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('17', 'Hari', 'E', '1995/01/14', '2018/08/28', '900000', '4','GANDhipuram');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`)
	 VALUES ('18', 'Raj', 'C', '1994/11/09', '2019/09/01', '1000000', '4','Karumathampatti');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('19', 'Tamil', 'V', '1997/12/06', '2018/10/31', '800000', '4','Arasur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`)
	 VALUES ('20', 'Anbu', 'B', '1997/10/24', '2018/06/03', '1000000', '4','Ganapathy');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('21', 'Harish', 'N', '1998/10/26', '2019/05/13', '1000000', '5','Ganapathy');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('22', 'Prasath', 'M', '1995/12/16', '2018/01/23', '800000', '5','Arasur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('23', 'yogaraj', 'P', '1999/02/06', '2017/02/03', '1000000', '5','Karumathampatti');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('24', 'Lokesh', 'T', '1995/04/28', '2016/06/08', '900000', '5','Thudiyalur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('25', 'yuvaraj', 'P', '1996/05/19', '2015/08/09', '1000000', '5','Thudiyalur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('26', 'Thulasi', 'H', '1994/06/15', '2014/05/08', '1000000', '6','Karumathampatti');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('27', 'Sasi', 'R', '1996/07/03', '2019/09/09', '1000000', '6','Ganapathy');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('28', 'Kannan', 'R', '1998/08/26', '2018/10/22', '900000', '6','Arasur');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('29', 'Arun', 'K', '1997/09/27', '2019/11/05', '1000000', '6','Karumathampatti');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`area`) 
	 VALUES ('30', 'Vijay', 'S', '1999/10/30', '2018/12/23', '900000', '6','Ganapathy');
     SELECT employee.first_name
           ,department_name 
	       ,area
       FROM exercise2.employee
           ,exercise2.department 
      WHERE employee.department_number=department.department_number 
        AND employee.area IN('Ganapathy');
  
     SELECT employee.first_name
           ,department_name 
	       ,area
       FROM exercise2.employee
           ,exercise2.department 
      WHERE employee.department_number=department.department_number 
        AND employee.area IN('arasur');
