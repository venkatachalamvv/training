CREATE SCHEMA exercise2;
CREATE TABLE `exercise2`.`employee` (
             `emp_id` INT NOT NULL
            ,`first_name` VARCHAR(45) NOT NULL
            ,`surname` VARCHAR(3) NOT NULL
            ,`dob` DATE NOT NULL
            ,`date_of_joining` DATE NOT NULL
            ,`annual_salary` INT NOT NULL
            ,`department_number` INT NOT NULL
            ,`emp_supv` INT NULL
			,PRIMARY KEY (`emp_id`)
            ,CONSTRAINT `emp_supv`
             FOREIGN KEY(emp_supv)REFERENCES employee(emp_id));
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
	 VALUES ('1', 'Ram', 'A', '1992/02/03', '2015/04/12', '1200000', '1');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('2', 'Mani', 'S', '2000/02/25', '2019/11/05', '1000000', '1','1');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('3', 'Praveen', 'D', '1999/05/17', '2016/10/12', '900000', '1','1');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('4', 'Rahul', 'F', '2000/12/12', '2019/04/16', '800000', '1','1');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('5', 'Manoj', 'G', '1996/09/05', '2016/03/02', '800000', '1','1');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
	 VALUES ('6', 'Venki', 'H', '1997/08/15', '2016/02/11', '1000000', '6');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('7', 'Naveen', 'J', '1996/06/25', '2017/01/12', '900000', '2', '6');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('8', 'Karthi', 'K', '1997/01/04', '2018/06/09', '1000000', '2','6');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('9', 'Thangavel', 'L', '1997/09/04', '2016/10/02', '800000', '2','6');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('10', 'Venkatesh', 'P', '1995/02/27', '2015/04/25', '900000', '2','6');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
	 VALUES ('11', 'Thiru', 'O', '1992/03/25', '2018/06/09', '1000000', '3');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('12', 'Murugan', 'I', '1996/05/05', '2019/06/19', '1000000', '3', '11');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('13', 'Venkat', 'A', '1997/05/04', '2017/06/29', '800000', '3', '11');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('14', 'Senthil', 'Y', '1997/03/14', '2018/05/08', '1000000', '3', '11');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('15', 'Gopi', 'T', '1998/02/04', '2016/06/01', '1000000', '3', '11');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
	 VALUES ('16', 'Guru', 'R', '1997/11/24', '2017/07/05', '900000', '4');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('17', 'Hari', 'E', '1995/01/14', '2018/08/28', '900000', '4', '16');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`)
	 VALUES ('18', 'Raj', 'C', '1994/11/09', '2019/09/01', '1000000', '4', '16');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('19', 'Tamil', 'V', '1997/12/06', '2018/10/31', '800000', '4', '16');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`)
	 VALUES ('20', 'Anbu', 'B', '1997/10/24', '2018/06/03', '1000000', '4', '16');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
	 VALUES ('21', 'Harish', 'N', '1998/10/26', '2019/05/13', '1000000', '5');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('22', 'Prasath', 'M', '1995/12/16', '2018/01/23', '800000', '5', '21');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('23', 'yogaraj', 'P', '1999/02/06', '2017/02/03', '1000000', '5', '21');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('24', 'Lokesh', 'T', '1995/04/28', '2016/06/08', '900000', '5', '21');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('25', 'yuvaraj', 'P', '1996/05/19', '2015/08/09', '1000000', '5', '21');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
	 VALUES ('26', 'Thulasi', 'H', '1994/06/15', '2014/05/08', '1000000', '6');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('27', 'Sasi', 'R', '1996/07/03', '2019/09/09', '1000000', '6','26');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('28', 'Kannan', 'R', '1998/08/26', '2018/10/22', '900000', '6','26');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('29', 'Arun', 'K', '1997/09/27', '2019/11/05', '1000000', '6','26');
INSERT INTO `exercise2`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`,`emp_supv`) 
	 VALUES ('30', 'Vijay', 'S', '1999/10/30', '2018/12/23', '900000', '6','26');
     SELECT a.emp_id AS "Emp_ID",a.first_name AS "Employee Name",
            b.emp_id AS "Supervisor ID",b.first_name AS "Supervisor Name"
       FROM exercise2.employee a, exercise2.employee b
      WHERE a.emp_supv = b.emp_id;
    