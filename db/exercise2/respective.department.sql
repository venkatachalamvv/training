     SELECT empl.first_name
           ,empl.surname
	       ,dept.department_name  
       FROM exercise2.employee empl 
           ,exercise2.department dept  
      WHERE empl.department_number = dept.department_number;
