SELECT employee.first_name AS fresher
      ,department_number AS department_name
  FROM exercise2.employee
 WHERE department_number IS NULL;
