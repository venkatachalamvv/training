     SELECT department.department_name 
           ,min(annual_salary) AS least_paid
           ,max(annual_salary) AS highest_paid
       FROM exercise2.employee 
		   ,exercise2.department
      WHERE department.department_number = employee.department_number
   GROUP BY department.department_number;
