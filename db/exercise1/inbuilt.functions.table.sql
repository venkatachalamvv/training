CREATE SCHEMA exercise1;
CREATE TABLE `exercise1`.`employee` (
             `employee_id` INT NOT NULL
            ,`first_name` VARCHAR(45) NOT NULL
            ,`surname` VARCHAR(45) NOT NULL
            ,`age` INT NOT NULL
            ,`city` VARCHAR(45) NOT NULL
            ,`salary` INT NOT NULL
            ,`department` VARCHAR(45) NOT NULL
			,PRIMARY KEY (`employee_id`));
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department,salary) 
	 VALUES ('1', 'Venki','V', '20', 'Salem', 'Electrical','550000');
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department,salary) 
	 VALUES ('2', 'Venky','P', '21', 'Salem', 'Electrical','600000');
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department,salary) 
	 VALUES ('3', 'Venkat','R', '19', 'Salem', 'Instrumentation','650000');
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department,salary) 
     VALUES ('4', 'Anbu','M', '22', 'Salem', 'Instrumentation','700000');
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department,salary) 
	 VALUES ('5', 'Hari','G', '20', 'Salem', 'Instrumentation','630000');
	 SELECT MIN(age)
		   ,MAX(age)
           ,MIN(salary)
           ,MAX(salary)
	   FROM exercise1.employee;
	 SELECT AVG(salary)
		   ,AVG(age)
	   FROM exercise1.employee;
	 SELECT COUNT(employee_id)
       FROM exercise1.employee;
	 SELECT SUM(salary)
       FROM exercise1.employee;
	 SELECT NOW();
	 SELECT employee_id
	       ,first_name
		   ,age
		   ,department
       FROM exercise1.employee
	  WHERE employee_id = ANY(SELECT employee_id FROM exercise1.employee WHERE age=20);