	 SELECT exercise1.employee.employee_id
           ,exercise1.department.department
           ,exercise1.project_detail.project
           ,exercise1.department.department_id
       FROM exercise1.employee
 INNER JOIN exercise1.department 
        ON exercise1.employee.department_id = exercise1.department.department_id
 INNER JOIN exercise1.project_detail 
         ON exercise1.employee.employee_id  = exercise1.project_detail.employee_id;
     SELECT exercise1.employee.employee_id
           ,exercise1.department.department
           ,exercise1.project_detail.project
           ,exercise1.department.department_id
       FROM exercise1.employee
 CROSS JOIN exercise1.department 
         ON exercise1.employee.department_id = exercise1.department.department_id
 CROSS JOIN exercise1.project_detail 
         ON exercise1.employee.employee_id  = exercise1.project_detail.employee_id; 
     SELECT exercise1.employee.employee_id
           ,exercise1.department.department
           ,exercise1.project_detail.project
           ,exercise1.department.department_id
       FROM exercise1.employee
  LEFT JOIN exercise1.department 
         ON exercise1.employee.department_id = exercise1.department.department_id
  LEFT JOIN exercise1.project_detail 
         ON exercise1.employee.employee_id  = exercise1.project_detail.employee_id;
     SELECT exercise1.employee.employee_id
           ,exercise1.department.department
           ,exercise1.project_detail.project
           ,exercise1.department.department_id
       FROM exercise1.employee
 RIGHT JOIN exercise1.department 
         ON exercise1.employee.department_id = exercise1.department.department_id
 RIGHT JOIN exercise1.project_detail 
         ON exercise1.employee.employee_id  = exercise1.project_detail.employee_id;