CREATE SCHEMA exercise1;
CREATE TABLE `exercise1`.`employee` (
			 `employee_id` INT NOT NULL
            ,`first_name` VARCHAR(45) NOT NULL
            ,`surname` VARCHAR(45) NOT NULL
            ,`age` INT NOT NULL
            ,`city` VARCHAR(45) NOT NULL
            ,`department` VARCHAR(45) NOT NULL
			,PRIMARY KEY (`employee_id`));
	   DESC exercise1.employee;
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department) 
	 VALUES ('1', 'Venki','V', '20', 'salem', 'Electrical');
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department) 
	 VALUES ('2', 'Venky','V', '21', 'salem', 'Electrical');
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department) 
	 VALUES ('3', 'Venkat','V', '19', 'salem', 'Instrumentation');
CREATE TABLE `exercise1`.`employee2` (
             `employee_id` INT NOT NULL
			,`first_name` VARCHAR(45) NOT NULL
			,`department` VARCHAR(45) NOT NULL
            ,PRIMARY KEY (`employee_id`));
	   DESC exercise1.employee2;
INSERT INTO exercise1.employee2 (employee_id, first_name, department) 
	 VALUES ('4', 'Naveen', 'Electrical');
INSERT INTO exercise1.employee2 (employee_id, first_name, department) 
	 VALUES ('5', 'Thangavelu', 'Instrumentation');
INSERT INTO exercise1.employee2 (employee_id, first_name,  department) 
	 VALUES ('3', 'Venkat', 'Instrumentation');
     SELECT employee_id
		   ,first_name
           ,department
       FROM exercise1.employee
      UNION ALL
     SELECT employee_id
		   ,first_name
           ,department
      FROM exercise1.employee2;
     SELECT employee_id
		   ,first_name
           ,department
       FROM exercise1.employee
      UNION DISTINCT
     SELECT employee_id
		   ,first_name
           ,department
       FROM exercise1.employee2;
     SELECT employee_id
		   ,first_name
           ,department
       FROM exercise1.employee
      UNION 
     SELECT employee_id
		   ,first_name
           ,department
       FROM exercise1.employee2;
