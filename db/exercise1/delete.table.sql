CREATE SCHEMA exercise1;
CREATE TABLE `exercise1`.`employee` (
             `employee_id` INT NOT NULL
            ,`first_name` VARCHAR(45) NOT NULL
            ,`surname` VARCHAR(45) NOT NULL
            ,`age` INT NOT NULL
            ,`city` VARCHAR(45) NOT NULL
            ,`department` VARCHAR(45) NOT NULL
            ,PRIMARY KEY (`employee_id`));
       DESC exercise1.employee;
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department) 
	 VALUES ('1', 'Venki','V', '20', 'salem', 'Electrical');
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department) 
	 VALUES ('2', 'Venky','V', '21', 'salem', 'Electrical');
INSERT INTO exercise1.employee (employee_id, first_name, surname, age, city, department) 
	 VALUES ('3', 'Venkat','V', '19', 'salem', 'Instrumentation');
 DESC TABLE exercise1.employee;
DELETE FROM exercise1.employee
	  WHERE employee_id = 3;
 DESC TABLE exercise1.employee;
DELETE FROM exercise1.employee
	  limit 1;
 DESC TABLE exercise1.employee;