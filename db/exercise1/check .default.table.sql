CREATE SCHEMA exercise1;
CREATE TABLE `exercise1`.`department` (
             `id` INT NOT NULL
             ,`name` VARCHAR(45) NOT NULL
			 ,PRIMARY KEY (`id`));
        DESC exercise1.department;
CREATE TABLE`exercise1`.`employee` (
            `employee_id` INT NOT NULL
		   ,`first_name` VARCHAR(45) NOT NULL
		   ,`surname` VARCHAR(45) NOT NULL
           ,`age` INT NOT NULL check(age>19)
           ,`city` VARCHAR(45) NOT NULL DEFAULT 'salem'
           ,`id` INT NOT NULL
           ,PRIMARY KEY (`employee_id`)
           ,INDEX `id_idx` (`id` ASC) VISIBLE
		   ,CONSTRAINT `id`
FOREIGN KEY (`id`)
 REFERENCES `exercise1`.`department` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE);
       DESC exercise1.employee;
INSERT INTO `exercise1`.`department` (`id`, `name`) 
	 VALUES ('1', 'electrical');
INSERT INTO `exercise1`.`department` (`id`, `name`) 
	 VALUES ('2', 'instrumentation');
INSERT INTO `exercise1`.`employee` (`employee_id`, `first_name`, `surname`, `age`, `city`, `id`) 
	 VALUES ('1', 'Venki', 'V', '20', '', '1');
INSERT INTO `exercise1`.`employee` (`employee_id`, `first_name`, `surname`, `age`, `city`, `id`)
	 VALUES ('2', 'Venkat', 'K', '21', '', '2');
INSERT INTO `exercise1`.`employee` (`employee_id`, `first_name`, `surname`, `age`, `city`, `id`) 
	 VALUES ('3', 'Venky', 'M', '22', '', '1');
	   DESC TABLE exercise1.department;
	   DESC TABLE exercise1.employee;
	   DROP DATABASE exercise1;