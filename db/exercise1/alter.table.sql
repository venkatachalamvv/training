CREATE SCHEMA  exercise1;
CREATE TABLE  `exercise1`.`employee` (
              `id` INT NOT NULL
             ,`name` VARCHAR(45) NOT NULL
             ,`phone_no` INT NOT NULL);
         DESC  exercise1.employee;
  ALTER TABLE `exercise1`.`employee` 
CHANGE COLUMN `name` `first_name` VARCHAR(45) NOT NULL ;
	     DESC  exercise1.employee;
  ALTER TABLE `exercise1`.`employee` 
  DROP COLUMN `phone_no`,
   ADD COLUMN `last_name` VARCHAR(45) NOT NULL AFTER `first_name`;
		 DESC  exercise1.employee;
  ALTER TABLE `exercise1`.`employee` 
   ADD COLUMN `phone_no` INT NOT NULL AFTER `last_name`;
         DESC  exercise1.employee;
  ALTER TABLE `exercise1`.`employee` 
CHANGE COLUMN `last_name` `surname` VARCHAR(45) NOT NULL ;
         DESC  exercise1.employee;

