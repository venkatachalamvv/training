CREATE VIEW employee_view AS
     SELECT employee.first_name AS first_name
	  	   ,department.department AS department
       FROM employee,department
      WHERE department.department_id = '2'
        AND employee.department_id = department.department_id
	