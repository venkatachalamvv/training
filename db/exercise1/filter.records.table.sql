	 SELECT employee_id
           ,first_name
           ,department
           ,age 
       FROM exercise1.employee
      WHERE city='Salem' 
	    AND (department='Instrumentation' OR department='Instrumentation');
	 SELECT employee_id
           ,first_name
           ,department
           ,age 
	   FROM exercise1.employee
	  WHERE not age < 20 ;
     SELECT employee_id
           ,first_name
           ,department
           ,age
	   FROM exercise1.employee
	  WHERE employee_id in ('4','2','5');
	 SELECT employee_id
           ,first_name
           ,department
           ,age 
	   FROM exercise1.employee
	  WHERE department like ('%l');
	 SELECT employee_id
           ,first_name
           ,department
           ,age 
	   FROM exercise1.employee
	  WHERE department like ('i%');
	 SELECT employee_id
           ,first_name
           ,department
           ,age 
	   FROM exercise1.employee
	  WHERE age like ('20');
 	 SELECT employee_id
           ,first_name
           ,department
           ,age
	   FROM exercise1.employee
	  WHERE first_name like ('v%i'); 
	 SELECT employee_id
           ,first_name
           ,department
           ,age
	   FROM exercise1.employee
      WHERE age =ANY (SELECT age FROM exercise1.employee WHERE age>20);
	 SELECT employee_id
           ,first_name
           ,department
           ,age 
	   FROM exercise1.employee
	  WHERE first_name like ('s_i');
 