SELECT SUM(amount) AS collected
          ,semester
          ,college.name
          ,university.university_name
      FROM semester_fee 
INNER JOIN student 
        ON student.id = semester_fee.stud_id 
INNER JOIN college 
        ON college.id = student.college_id
INNER JOIN university
        ON university.univ_code = college.univ_code 
	   AND semester_fee.paid_status = 'paid' 
  GROUP BY college.name,semester_fee.semester;
SELECT SUM(amount) AS uncollected
	      ,semester
          ,college.name
          ,university.university_name
      FROM semester_fee 
INNER JOIN student
        ON student.id = semester_fee.stud_id 
INNER JOIN college
        ON college.id = student.college_id
INNER JOIN university 
        ON university.univ_code = college.univ_code 
	   AND semester_fee.paid_status = 'unpaid'
  GROUP BY college.name,semester_fee.semester;
  SELECT SUM(amount) AS collected
          ,semester
          ,university.university_name
      FROM semester_fee 
INNER JOIN student 
        ON student.id = semester_fee.stud_id 
INNER JOIN college 
        ON college.id = student.college_id
INNER JOIN university
        ON university.univ_code = college.univ_code 
	   AND semester_fee.paid_status = 'paid' 
  GROUP BY university.univ_code,semester_fee.semester;