SELECT  employee.name
	   ,email
       ,college.name
       ,city
       ,dept_name
       ,designation.name
   FROM employee
INNER JOIN college 
        ON college.id = employee.college_id
INNER JOIN university 
        ON university.univ_code = college.univ_code
       AND university.university_name = 'anna'
INNER JOIN college_department 
        ON college_department.cdept_id = employee.cdept_id
INNER JOIN designation  
        ON designation.rank = employee.desig_id
INNER JOIN department
        ON department.dept_code = college_department.udept_code
  ORDER BY college.name,designation.rank,department.dept_name