SELECT  code
       ,college.name AS college_name
       ,university_name 
       ,college.city
       ,college.state
       ,college.year_opened
       ,dept_name AS department_name
       ,employee.name AS hod_name
   FROM college
INNER JOIN university 
        ON university.univ_code = college.univ_code
INNER JOIN department 
        ON department.univ_code = college.univ_code 
INNER JOIN college_department 
        ON college_department.udept_code=department.dept_code            
       AND (department.dept_name = "cse" OR department.dept_name ="it")
INNER JOIN employee 
        ON employee.cdept_id=college_department.cdept_id 
       AND college_department.college_id=college.id 
       AND employee.desig_id='2'
  ORDER BY code


