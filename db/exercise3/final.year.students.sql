SELECT  roll_number
       ,student.name
       ,gender
       ,dob
       ,email
       ,phone
       ,address
       ,college.name AS college_name
       ,department.dept_name AS dpartment_name
   FROM student
INNER JOIN college 
        ON college.id = student.college_id
       AND college.city = 'Coimbatore'
INNER JOIN university 
        ON university.univ_code = college.univ_code
       AND university.university_name = 'anna'
INNER JOIN college_department
        ON college_department.cdept_id = student.cdept_id
       AND student.academic_year = 2017
INNER JOIN department
        ON department.dept_code = college_department.udept_code

