SELECT  roll_number
       ,student.name
       ,gender
       ,dob
       ,email
       ,phone
       ,address
       ,college.name AS college_name
       ,semester
       ,grade
       ,GPA
       ,credits
   FROM student
INNER JOIN college 
        ON college.id = student.college_id
INNER JOIN semester_result 
        ON semester_result.stud_id = student.id
  ORDER BY college.name,semester
     LIMIT 70 OFFSET 45

