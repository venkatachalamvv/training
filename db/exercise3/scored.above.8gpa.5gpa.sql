SELECT  roll_number
       ,student.name
       ,gender
       ,dob
       ,email
       ,phone
       ,address
       ,college.name AS college_name
       ,semester
       ,grade
       ,GPA
   FROM student
INNER JOIN college 
        ON college.id = student.college_id
INNER JOIN semester_result 
        ON semester_result.stud_id = student.id
	   AND semester_result.GPA >= 8
  ORDER BY college.name,semester;
    SELECT roll_number
          ,student.name
          ,gender
          ,dob
          ,email
          ,phone
          ,address
          ,college.name AS college_name
          ,semester
          ,grade
          ,GPA
      FROM student
INNER JOIN college 
        ON college.id = student.college_id
INNER JOIN semester_result 
        ON semester_result.stud_id = student.id
	   AND semester_result.GPA >= 5
  ORDER BY college.name,semester
