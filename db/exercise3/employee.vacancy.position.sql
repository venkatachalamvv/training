      SELECT designation.name
            ,designation.rank
            ,department.dept_name
            ,college.name
            ,college.city
            ,university.university_name
        FROM designation
  INNER JOIN employee 
          ON employee.desig_id = designation.id 
		 AND employee.name = ''
  INNER JOIN college_department 
          ON college_department.cdept_id = employee.cdept_id 
  INNER JOIN college
          ON college.id = employee.college_id 
  INNER JOIN department
          ON  department.dept_code = college_department.udept_code  
  INNER JOIN university 
          ON university.univ_code = college.univ_code
    ORDER BY designation.rank