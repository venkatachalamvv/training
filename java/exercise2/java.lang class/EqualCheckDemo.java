/*
Requirement : demonstrate object equality using Object.equals() vs ==, using String objects
Entiy : EqualCheckDemo
Function Declaartion : ublic static void main(String[] args) 
Jobs To Be Done :1.Declare the class EqualCheckDemo 
                 2.Declare the public static void main(String[] args)
                 3.Call the functions from that classes and print the statement.  */
*/

public class EqualCheckDemo { 

    public static void main(String[] args) 
    { 
        String string1 = new String("Program"); 
        String string2 = new String("Program");
        System.out.println(string1 == string2); 
        System.out.println(string1.equals(string2)); 
    } 
}