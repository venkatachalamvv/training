/*
Requirement:print the absolute path of the .class file of the current class

Entiy : PathDemo

Function Declaartion:public static void main(final String[] args)

Jobs To Be Done : 1.Declare the class PathDemo 
                  2.Declare the public static void main(String[] args)
                  3.Call the functions from that classes and print the absolute path
*/
public class PathDemo
{
    public static void main(String[] args)
    {
    
        System.out.println("current path : " +System.getProperty("user.dir" ));
    }
} 