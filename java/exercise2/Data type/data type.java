/* What is the value of the following expression, and why?
     Integer.valueOf(1).equals(Long.valueOf(1))    */
	 
Solution:

    Value of the expression - false
    Both values differ in their data type. 
	
/* print the type of the result value of following expressions */
  - 100 / 24
  - 100.10 / 10
  - 'Z' / 2
  - 10.5 / 0.5
  - 12.4 % 5.5
  - 100 % 56
	 
Solution:
    - 4
    - 10.01
    - 45
    - 21.0
    - 1.4000000000000004
    - 44
	
/*To invert the value of a boolean, which operator would you use? */


class BooleanDemo{
    
	public static void main(String[] args){
	    boolean value = true;
		System.out.println(!value);
	}
}

/* Solution:
    ! - can be used to invert a boolean value */