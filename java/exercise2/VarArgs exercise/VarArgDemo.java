/*Requirement : demonstrate overloading with varArgs
Entity : VarargDemo ,VariableArgument
Function declaration : public static void main(String[] args)
                      public void printNumbers(int... number)
Jobs to be done :1)The main class an object is cretaed for varaiableargument class
                 2)Then the method printNumbers  is called in the main method with two parameter
                 3)Again the method printNumbers  is called in the main method with six parameter where
                  overloading is acheived.
                 4)In the method print number the numbers are printed
*/

class VariableArgument{
    public void printNumbers(int... number) {
        System.out.println("The numbers are:");
        for(int i : number) {
        System.out.println(i);
        }
    }
   
}
public class VarArgDemo{
    public static void main(String[] args) {
        
        VariableArgument variableArgument = new VariableArgument();
        variableArgument.printNumbers(10,55);
        variableArgument.printNumbers(111,152,156,158,509,524);
    
    }
}