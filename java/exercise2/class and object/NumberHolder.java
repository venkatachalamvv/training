    /*Requirement:
    Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }               */

    /*Entities = NumberHolder */

    /*Function Declaration : No function declaration in this program */
    /*Jobs To Be Done:
    1)Considering the class and given program.
    2)Create the object numberHolder for the class NumberHolder
    3)Display the instance variable values */

public class NumberHolder {
	 
    public int anInt;
    public float aFloat;
	
    public static void main(String[] args) {
		
    NumberHolder numberHolder = new NumberHolder();
    numberHolder.anInt = 123;
    numberHolder.aFloat = 11.1f;
    System.out.println(numberHolder.anInt);   
    System.out.println(numberHolder.aFloat);   
    }
}