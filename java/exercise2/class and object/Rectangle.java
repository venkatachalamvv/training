    /*Requirement:
    What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }                                        
    }                              */
    /* Entities = SomethingIsWrong */

    /* Function Declaration : area() is function */

    /* Jobs To Be Done:
        1)Considering the class and given program.
        2)To check error in the given program.
        3)To change class name from SomethingIsWrong to Rectangle.
        4)Create the method area() and print the product of width and height. */
 
    public class Rectangle {
		
    public int width;
    public int height;
    public  int area(){
        return (width * height);
    }
    public static void main(String[] args) {
		
        Rectangle rectangle = new Rectangle();
        rectangle.width = 40;
        rectangle.height = 50;
        System.out.println("myRect's area is " + rectangle.area());
    }
}