/*Requirement:
    compare the enum values using equal method and == operator
Entity:
    Months
    Enum
Function Declaration:
    valueOf()
Job to be done:
    1)consider the class Enum and enum Months for the following program.
    2)To declare the Months in the enum Months.
    3)In the class enum main method() to create a object day and enter the value "MAY has 31 Months".
    4)To print required output. */
	
public class EnumDemo {
   
    public static void main(String[] args) {
       
          Months month = Months.valueOf("MAY");
      
        if(month.equals (Months.JANUARY)) {
            
            System.out.println("JANUARY has 31 Months");
        
        }else if(Months.FEBRUARY == month) {
           
           System.out.println("FEBRUARY has 29 Months");
        
        } else if(Months.MARCH == month) {
           
           System.out.println("MARCH has 31 Months");
        
        }else if(month.equals(Months.APRIL)) {
            
            System.out.println("APRIL has 30 Months");
       
       }else if(Months.MAY == month) {
        
        System.out.println("MAY has 31 Months");
        
        }       
    }
}
 enum Months {JANUARY
             ,FEBRUARY
             ,MARCH
             ,APRIL
             ,MAY
             }