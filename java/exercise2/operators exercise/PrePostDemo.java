    /* Requirement : In the following program, explain why the value "6" is printed twice in a row 
	class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }               */
    /*  Entities = PrePostDemo
	
    /*  Function Declaration:No function declaration in this program */
	
    /*Jobs To Be Done: Check the value "6" is printed twice in a row. */
     
	 
	 
	class PrePostDemo {
        public static void main(String[] args){
            int i = 3;
            i++;
            System.out.println(i);    // "4"
            ++i;
            System.out.println(i);    // "5"
            System.out.println(++i);  // "6"
            System.out.println(i++);  // "6"
            System.out.println(i);    // "7"
           }
       }
	/* Explanation:

    System.out.println(++i);  // "6"   Used as prefix on a variable, the value of variable gets incremented by 1
    System.out.println(i++);  // "6"   Used as postfix on a variable, the value of variable is first returned and then gets incremented by 1 
	                                   So "7" doesn't get printed       */