/* What is wrong with the following interface? and fix it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }


Solution:
    It has a method implementation in it. Only default and static methods have implementations. 
	
	public interface SomethingIsWrong {
    default void aMethod(int aValue) {
        System.out.println("Hi Mom");
    }
}                              */

/*  Is the following interface valid?
    public interface Marker {}

Solution:
     
    Methods are not required. 
	Empty interfaces can be used as types.    */
	
/*  What methods would a class that implements the java.lang.CharSequence interface have to implement?

Solution:

    charAt, length, subSequence, and toString.  */