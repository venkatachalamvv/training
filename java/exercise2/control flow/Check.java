    /* Requirement:
    What output do you think the code will produce if aNumber is 3?
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string"); */

    /* Entities :  Doesn't have any class */

    /* Function Declaration : No function declaration in this program */
    /* Jobs To Be Done : 
        1)Checking the aNumber value greater than or equal to zero.
        3)If it is true then moves to the next statement.
        4)Checking the aNumber value equal to zero. 
        5)If it is false then  moves to the else part.
        6)It prints the second string and third string.  */
	
class Check {
    
    public static void main (String args[]) {
        
        int aNumber = 3;
        if (aNumber >= 0) {
            
            if (aNumber == 0) {
                
                System.out.println("first string");
            }
            else {
            
                System.out.println("second string");
            }
        }
    System.out.println("third string");
    }
}