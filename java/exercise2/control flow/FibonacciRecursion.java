    /* Requirement : print fibinocci using for loop, while loop and recursion */
	
    /* Entities : FibonacciRecursion,FibonacciForLoop and FibonacciWhile has been given. */

    /* Function Declaration : printRecursion(n) is the function declared in the FibonacciRecursion . */

    /* Jobs to be Done:
    1. Declare the class FibonacciRecursion ,FibonacciForLoop and FibonacciWhile.
    2. Asign the variable as a ,b ,c ,count and i.
    3. Fibonacci using for loop, while loop and recursion to solved in this program.
    4. print the Fibonacci series using print statement. */

public class FibonacciRecursion {
    public static int a = 0 ,b = 1 ,c ;
    public static void printRecursion(int i) {
        if(i > 0) {
                c = a + b;
                a = b;
                b = c;
                System.out.print(a + " ");
                printRecursion(i - 1);
        }
    }
    public static void main(String[] args) {
        int n = 10;
        printRecursion(n);
    }
}

