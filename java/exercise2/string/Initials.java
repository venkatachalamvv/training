    /* Requirement : Write a program that computes your initials from your full name and displays them.*/
  
    /* Entities : Initials */

    /* Function Declaration :  public static void main(String[] args)  */

    /* Jobs To Be Done: 1)Considering the following program.
	                    2)To declare and assign the Variable myName.
						3)To get the length of the variable and check the character is uppercase. 
                        4)To print the uppercase character of the variable as Initials.	. */
	
public class Initials {
    public static void main(String[] args) {
        String myName = "Venktachalam Kailasam";
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();

        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(myName.charAt(i))) {
                myInitials.append(myName.charAt(i));
            }
        }
        System.out.println("My initials are: " + myInitials);
    }
}