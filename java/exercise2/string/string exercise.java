/*What is the initial capacity of the following string builder?
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");

Solution:
Length of the given String = 26
Initial capacity of the string = 26 + 16 = 42 */
	
	
/* How long is the string returned by the following expression? What is the string?
      "Was it a car or a cat I saw?".substring(9, 12)

Solution:
car                               */


/* Consider the following string:
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.
Solution:
1.32
2.e
3.hannah.charAt(15)             */


/* Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";

Solution:
 hi.concat(mom) 
 hi + mom */