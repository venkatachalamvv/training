    /* Requirement : sort and print following String[] alphabetically ignoring case.
	Also convert and print even indexed Strings into uppercase
    { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }*/
	
    /* Entities : Sort */

    /* Function Declaration :  No function declaration in this program  */

    /* Jobs to be Done:Create an array and array is sorted using sort (). 
	                   If the index of the array is divisible by two, then it's converted into upper case */
import java.util.Arrays; 
 class Sort{
    public static void main(String[] args){
        String [] array = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(array,String.CASE_INSENSITIVE_ORDER);
        System.out.println(Arrays.toString(array));
        for(int i=0;i<array.length;i++)
        {
            if(i%2==0) {
                array[i] = array[i].toUpperCase();
            }
        }
        System.out.print(Arrays.toString(array));
    }
}
