/*
Entity:
    InheritanceDemo
Function Declaration:
    public static void main(String[] args)

*/
public class InheritanceDemo{
    public static void main(String[] args) {
        Dog dog = new Dog();
        Snake snake = new Snake();
        Cat cat = new Cat();
        Animal animal = new Animal();
        dog.sound();
        snake.sound();
        cat.sound();
        animal.sound();
        animal.run();
        animal.run(2);
    }
}