/*
Entities:
    Cat.

Function Declaration:
    public void sound().

*/

public class Cat extends Animal {
    public void sound() {
        System.out.println("the cat meows");
    }
}