/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entities:
    Animal,InheritanceDemo

Function Declaration:
    public void sound()
    public void run()
    public void run(int kilometer)
	public static void main(String[] args)

Jobs to be Done:
    1. Declare the class Animal,InheritanceDemo 
    2. Declare the subclass Dog, Cat and Snake
    3. Declare the function public void sound() and public void run().
    4. Call the functions from that classes and print the statement.  */

public class Animal {
    public void sound() {
        System.out.println(" This is the parent class");
    }
    
    public void run() {
        System.out.println("The animal is running");
    }
    
    public void run(int kilometer) {
        System.out.println("The animal is running at maximum distance");
    }
}