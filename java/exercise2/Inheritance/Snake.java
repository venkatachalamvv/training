/*
Entities:
    Snake.

Function Declaration:
    public void sound().

*/

public class Snake extends Animal {
    public void sound() {
        System.out.println("the snake crawls");
    }
}