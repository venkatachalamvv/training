/*Requirement:
      Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively
Entity:
    Shape,Square,Circle,AbstractDemo
Function Declaration:
    public void printArea() ,  public void printPerimeter() ,  public static void main(String[] args)
Jobs to be done:
    1) Declare the abstract class Shape
    2) Create the subclass Square ,Circle and Declare the class AbstractDemo
    3) Declare the function public void printArea() and public void printPerimeter()
	4) Print the statement as given
*/

import java.util.*;
abstract class Shape{
    public abstract void printArea();
    public abstract void printPerimeter();
}
class Square extends Shape {
    private double side;
    public Square(double side) {
    
        this.side = side;
    
    }
    public void printArea() {
    
        System.out.println("area of square = " + (side*side));

    }
    public void printPerimeter() {
    
        System.out.println("perimeter of square = " + (4 * side));
    
    }
}
class Circle extends Shape {
    private double radius; 
    public Circle(double radius) {
   
        this.radius = radius;
    
    }  
    public void printArea() {
    
        System.out.println("area of circle is = " + (3.14 * radius * radius));
    
    } 
    public void printPerimeter() {
    
        System.out.println("perimeter of circle = " + (2 * 3.14 * radius));
    
    }
}
public class AbstractDemo {
    public static void main(String[] args) {
    
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the lenght of the side of the square:");
        double side = scanner.nextDouble();
        System.out.print("Enter the radius of the circle:");
        double radius = scanner.nextDouble();
        Square square = new Square(side);
        square.printArea();
        square.printPerimeter();
        Circle circle = new Circle(radius);
        circle.printArea();
        circle.printPerimeter();
   
   }
}