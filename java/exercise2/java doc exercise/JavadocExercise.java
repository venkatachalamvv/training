/*
Use the Java API documentation for the Box class (in the javax.swing package) to help you answer the following questions.
    - What static nested class does Box define?
    - What inner class does Box define?
    - What is the superclass of Box's inner class?
    - Which of Box's nested classes can you use from any class?
    - How do you create an instance of Box's Filler class?
*/
Solution:
-Box.Filler
-Box.AccessibleBox
-[java.awt.]Container.AccessibleAWTContainer
-Box.Filler
-new Box.Filler(minDimension, prefDimension, maxDimension)


/*
What methods would a class that implements the java.lang.CharSequence interface have to implement?
*/
Solution:
charAt, length, subSequence, and toString.


/*
What Integer method can you use to convert an int into a string that expresses the number in hexadecimal?
  For example, what method converts the integer 65 into the string "41"?
*/
Solution:
toHexString


/*
What Integer method would you use to convert a string expressed in base 5 into the equivalent int?
  For example, how would you convert the string "230" into the integer value 65? Show the code you would use to accomplish this task.
*/
Solution:
String base5String = "230";
int result = Integer.valueOf(base5String, 5);


/*
What Double method can you use to detect whether a floating-point number has the special value Not a Number (NaN)?
*/
Solution:
isNan


