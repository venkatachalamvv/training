import com.industry.training.core.Employee;

public class Department extends Employee{
	
	public int departmentId;
	public String departmentName;
	private int experience;
	
	public Department(int departmentId,String departmentName,int experience) {
	    
	    this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.experience = experience;
	}
	public Department() {
		
		departmentId= 0;
		departmentName= "NA";
		experience = 0;
	}
	public void printinfo() {
	 
		System.out.println("departmentId: " + departmentId);
        System.out.println("departmentName: " + departmentName);
		System.out.println("experience: " + experience);
	}
	public static void main(String[] args) {
		Department department = new Department();
        Department department1 = new Department(1,"Electrical",2);
		department.printinfos();
		department1.printinfo();
		department.printinfo();
	}
}