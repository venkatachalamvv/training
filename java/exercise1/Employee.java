package com.industry.training.core;

public class Employee {
	
    public String name;
    public int id;
	protected int age ;
	public String gender;
	
	public Employee(String name,int id,int age,String gender) {    // parameterised constructor
	
	    this.name = name;
		this.id = id;                                         
		this.age = age;
		this.gender = gender;
	}
	public Employee() {   // Non-parameterised constructor
		
		name = "NA";
		id = 0;
		age = 0;
		gender ="NA";
	}
 public void printinfos() {
	 
        System.out.println("Name :" + name);
        System.out.println("ID :" + id);
        System.out.println("Age :" + age);
        System.out.println("Gender :" + gender);
    }
public static void main(String[] args) {
	
        Employee employee = new Employee();
        Employee employee1 = new Employee("Venkat",101,22,"m");
        Employee employee2 = new Employee("Venky",102,23,"m");
        Employee employee3 = new Employee("Venki",103,24,"m");
        employee. printinfos();
		employee1.printinfos();
		employee2.printinfos();
		employee3.printinfos();
	}
}
 